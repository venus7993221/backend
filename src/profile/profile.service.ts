import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { BlockList, ProfileUpdateDto } from 'src/profile/profile.dto';
import { ImageService } from 'src/shared/image.service';
import { SupaClient } from 'src/shared/supabaseClient';
import { Json } from 'src/types/supabase';

@Injectable()
export class ProfileService {
  constructor(
    private supaClient: SupaClient,
    private imageService: ImageService,
  ) {}

  async getProfileById(profileId: string) {
    const result = await this.supaClient
      .getClient()
      .from('user_profiles')
      .select('id, avatar, name, user_name, about_me, is_verified')
      .eq('id', profileId)
      .limit(1)
      .single();

    if (result.error) {
      console.error(result.error, profileId)
    }

    return result.data;
  }

  async getFullProfileById(profileId: string) {
    const result = await this.supaClient
      .getClient()
      .from('user_profiles')
      .select('*')
      .eq('id', profileId)
      .limit(1)
      .single();

    if (result.error) {
      console.error(result.error, profileId)
    }

    return result.data;
  }

  async getBlockedContent(profileId: string) {
    const currentProfile = await this.getFullProfileById(profileId);
    if (!currentProfile) {
      throw new HttpException('Profile not found', HttpStatus.NOT_FOUND);
    }

    const blockList = (currentProfile.block_list as unknown as BlockList) || {
      bots: [],
      creators: [],
      tags: [],
    };

    const botQuery = this.supaClient
      .getClient()
      .from('characters')
      .select('id, name, description, is_nsfw')
      .eq('is_public', true)
      .in('id', blockList.bots);

    const creatorQuery = this.supaClient
      .getClient()
      .from('user_profiles')
      .select('id, name, user_name, avatar')
      .in('id', blockList.creators);

    const [botResult, creatorResult] = await Promise.all([
      botQuery,
      creatorQuery,
    ]);

    return {
      bots: botResult.data ?? [],
      creators: creatorResult.data ?? [],
      tags: blockList.tags, // show this on clientSide
    };
  }

  async updateProfile(profileId: string, payload: ProfileUpdateDto) {
    const currentProfile = await this.getFullProfileById(profileId);
    if (!currentProfile) {
      throw new HttpException('Profile not found', HttpStatus.NOT_FOUND);
    }

    if (
      currentProfile.user_name &&
      payload.user_name &&
      currentProfile.user_name !== payload.user_name
    ) {
      throw new HttpException(
        'Can not update username',
        HttpStatus.BAD_REQUEST,
      );
    }

    let blockList = payload.block_list;
    // Set default blocklist if possible
    if (!currentProfile.block_list && !blockList) {
      blockList = {
        bots: [],
        creators: [],
        tags: [],
      };
    }

    const result = await this.supaClient
      .getClient()
      .from('user_profiles')
      .update({
        about_me: payload.about_me,
        avatar: payload.avatar,
        user_name: payload.user_name,
        name: payload.name,
        profile: payload.profile,
        config: payload.config,
        block_list: blockList as unknown as Json | undefined,
      })
      .eq('id', profileId);

    if (result.error && result.error.code === '23505') {
      throw new HttpException(
        `Username "${payload.user_name}" already existed. Please choose another one!`,
        HttpStatus.BAD_REQUEST,
      );
    }

    if (payload.avatar && payload.avatar !== currentProfile.avatar) {
      await this.imageService.syncImageToLocal('avatars', payload.avatar);
    }

    return true;
  }
}
