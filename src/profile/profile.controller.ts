import { CacheInterceptor, CacheTTL } from '@nestjs/cache-manager';
import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { SupabaseJWTGuard } from 'src/auth/supabase.guard';
import { GetUserId } from 'src/auth/user.decorator';
import {
  BlockedContent,
  ProfileResponse,
  ProfileUpdateDto,
} from 'src/profile/profile.dto';
import { CacheManagerService } from 'src/shared/cache-manager.service';
import { TEN_MINUTE_TTL } from 'src/shared/const';
import { ProfileService } from './profile.service';

@Controller('profiles')
@ApiTags('profiles')
export class ProfileController {
  constructor(
    private readonly profileService: ProfileService,
    private cacheManagerService: CacheManagerService,
  ) {}

  @Patch('/mine')
  @ApiBearerAuth()
  @ApiResponse({ type: ProfileResponse })
  @UseGuards(SupabaseJWTGuard)
  async updateProfile(
    @GetUserId() profileId: string,
    @Body() payload: ProfileUpdateDto,
  ) {
    const result = await this.profileService.updateProfile(profileId, payload);
    this.cacheManagerService.clearProfileCache(profileId);

    return result;
  }

  @Get('/mine')
  @ApiBearerAuth()
  @ApiResponse({ type: ProfileResponse })
  @UseGuards(SupabaseJWTGuard)
  async getMyProfile(@GetUserId() profileId: string) {
    const profile = await this.cacheManagerService.getFromCache(
      `full_profile_${profileId}`,
      () => this.profileService.getFullProfileById(profileId),
      TEN_MINUTE_TTL,
    );

    return profile;
  }

  @Get('/mine/blocked')
  @ApiBearerAuth()
  @ApiResponse({ type: BlockedContent })
  @UseGuards(SupabaseJWTGuard)
  async getBlockedContent(@GetUserId() profileId: string) {
    const blockedContent = await this.profileService.getBlockedContent(
      profileId,
    );

    return blockedContent;
  }

  @Get('/:id')
  @ApiResponse({ type: ProfileResponse })
  @UseInterceptors(CacheInterceptor)
  @CacheTTL(TEN_MINUTE_TTL) // 10 minutes
  getProfile(@Param('id') profileId: string) {
    // this.cacheManagerService.printKeys();
    return this.profileService.getProfileById(profileId);
  }
}
