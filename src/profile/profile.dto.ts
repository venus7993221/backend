import { IsNotEmpty, IsObject, IsOptional, IsString } from 'class-validator';
import { SupaJson } from 'src/types/type-alias';

export class BlockList {
  bots: string[];
  creators: string[];
  tags: number[];
}

export class ProfileUpdateDto {
  @IsString()
  @IsOptional()
  about_me?: string;

  @IsString()
  @IsOptional()
  avatar?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  name?: string;

  @IsString()
  @IsOptional()
  profile?: string;

  @IsString()
  @IsOptional()
  user_name?: string;

  @IsOptional()
  @IsObject()
  config?: SupaJson;

  @IsOptional()
  @IsObject()
  block_list?: BlockList;
}

export class ProfileResponse {
  id: string;
  about_me: string;
  avatar: string;
  name: string;
  user_name: string;
  is_verified: boolean;
}

class BlockedContentBot {
  id: string;
  name: string;
  description: string;
  is_nsfw: boolean;
}
class BlockedContentCreator {
  id: string;
  name: string;
  user_name: string;
  avatar: string;
}
export class BlockedContent {
  bots: BlockedContentBot[];
  creators: BlockedContentCreator[];
  tags: number[];
}

export class FullProfileResponse extends ProfileResponse {
  profile: string;
  config: SupaJson;
}
