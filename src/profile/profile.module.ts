import { Module } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { ProfileController } from './profile.controller';
import { CharacterModule } from 'src/character/character.module';

@Module({
  imports: [CharacterModule],
  controllers: [ProfileController],
  providers: [ProfileService],
})
export class ProfileModule {}
