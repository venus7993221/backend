import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { SupaClient } from 'src/shared/supabaseClient';
import { CreateReviewDto, ReviewView } from './review.dto';

@Injectable()
export class ReviewService {
  private readonly logger = new Logger(ReviewService.name);

  constructor(private supaClient: SupaClient) {}

  async getReviewsByCharacterId(characterId: string) {
    const responses = await this.supaClient
      .getClient()
      .from('reviews')
      .select('*, user_profiles(avatar, name, user_name)')
      .order('created_at', { ascending: false })
      .eq('character_id', characterId)
      .returns<ReviewView[]>();

    const reviews = responses.data || [];
    return reviews;
  }

  async create(userId: string, payload: CreateReviewDto) {
    const result = await this.supaClient
      .getClient()
      .from('reviews')
      .insert({
        user_id: userId,
        character_id: payload.character_id,
        is_like: payload.is_like,
        content: payload.content ? payload.content : null,
      })
      .select()
      .single();

    if (result.error) {
      this.logger.error(result.error);
      throw new HttpException('Can not create review.', HttpStatus.BAD_REQUEST);
    }

    return result.data;
  }
}
