import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export class CreateReviewDto {
  @IsString()
  @IsNotEmpty()
  @IsUUID()
  character_id: string;

  @IsNotEmpty()
  @IsBoolean()
  is_like: boolean;

  @IsString()
  @IsOptional()
  content?: string;
}

export class Review {
  character_id: string;
  content: string | null;
  created_at: string;
  is_like: boolean;
  user_id: string;
}

export class ReviewView extends Review {
  user_profiles: {
    avatar: string;
    name: string;
    user_name: string;
  };
}
