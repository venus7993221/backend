import { Controller, Post, Body, UseGuards, Get, Param } from '@nestjs/common';
import { ReviewService } from './review.service';
import { CreateReviewDto, Review, ReviewView } from './review.dto';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { SupabaseJWTGuard } from 'src/auth/supabase.guard';
import { GetUserId } from 'src/auth/user.decorator';

@Controller('reviews')
@ApiTags('reviews')
@Controller('review')
@ApiBearerAuth()
export class ReviewController {
  constructor(private readonly reviewService: ReviewService) {}

  @Get('/:id')
  @ApiResponse({ type: ReviewView, isArray: true })
  get(@Param('id') characterId: string) {
    return this.reviewService.getReviewsByCharacterId(characterId);
  }

  @Post()
  @UseGuards(SupabaseJWTGuard)
  @ApiResponse({ type: Review })
  create(
    @GetUserId() userId: string,
    @Body() createReviewDto: CreateReviewDto,
  ) {
    return this.reviewService.create(userId, createReviewDto);
  }
}
