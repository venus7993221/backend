import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SupabaseGuard, SupabaseJWTGuard } from 'src/auth/supabase.guard';
import { GetUser } from 'src/auth/user.decorator';
import { JwtUser } from 'src/types/jtw-user';
import { AppService } from './app.service';

@Controller()
@ApiTags('test')
@ApiBearerAuth()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/env')
  getEnv() {
    return { env: process.env.NODE_ENV || 'local' };
  }

  @UseGuards(SupabaseGuard)
  @Get('/test-profile')
  getProfile(@Req() req: any) {
    return req.user;
  }

  @UseGuards(SupabaseJWTGuard)
  @Get('/test-profile-jwt')
  getProfileJwt(@GetUser() user: JwtUser) {
    return user;
  }
}
