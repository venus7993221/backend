import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { JwtUser } from 'src/types/jtw-user';

export const GetUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request: Express.Request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);

export const GetUserId = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request: Express.Request = ctx.switchToHttp().getRequest();
    const user = request.user as JwtUser | undefined;
    return user ? user.sub : undefined;
  },
);
