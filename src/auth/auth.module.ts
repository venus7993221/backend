import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { SupabaseJWTStrategy } from 'src/auth/supabase.jwt.strategy';
import { SupabaseStrategy } from 'src/auth/supabase.strategy';

@Module({
  imports: [PassportModule],
  providers: [SupabaseStrategy, SupabaseJWTStrategy],
  exports: [SupabaseStrategy, SupabaseJWTStrategy],
})
export class AuthModule {}
