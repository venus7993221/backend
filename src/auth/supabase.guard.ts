import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class SupabaseGuard extends AuthGuard('supabase') {}

@Injectable()
export class SupabaseJWTGuard extends AuthGuard('supabase-jwt') {}

// Use this when we want auth check to be optional
@Injectable()
export class SupabaseJWTGuardOptional extends AuthGuard('supabase-jwt') {
  handleRequest(err: any, user: any) {
    return user;
  }
}
