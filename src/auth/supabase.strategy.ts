import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-strategy';
import { ExtractJwt, JwtFromRequestFunction } from 'passport-jwt';
import { SupabaseAuthUser } from 'nestjs-supabase-auth';
import { SupaClient } from 'src/shared/supabaseClient';

@Injectable()
export class SupabaseStrategy extends PassportStrategy(Strategy, 'supabase') {
  private extractor: JwtFromRequestFunction;

  public constructor(private supaClient: SupaClient) {
    super();

    this.extractor = ExtractJwt.fromAuthHeaderAsBearerToken();
  }

  async validate(payload: SupabaseAuthUser): Promise<SupabaseAuthUser> {
    return payload;
  }

  authenticate(req: any): void {
    const idToken = this.extractor(req);

    if (!idToken) {
      this.fail('UNAUTHORIZED', 401);
      return;
    }

    // Maybe add some caching here?
    const client = this.supaClient.getClient();
    client.auth
      .getUser(idToken)
      .then((result) => {
        if (result) {
          this.success(result, {});
          return;
        }
        this.fail('UNAUTHORIZED', 401);
      })
      .catch((err) => {
        this.fail(err.message, 401);
      });
  }
}
