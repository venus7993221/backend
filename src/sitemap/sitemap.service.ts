import { Injectable } from '@nestjs/common';
import { SupaClient } from 'src/shared/supabaseClient';

interface Char {
  id: string;
  name: string;
  creator_id: string;
}

interface Creator {
  id: string;
  name: string;
  user_name: string;
}

@Injectable()
export class SitemapService {
  constructor(private supaClient: SupaClient) {}

  public async getAllPublicBots() {
    const publicCharacterCountQuery = await this.supaClient
      .getClient()
      .from('characters')
      .select('id', { head: true, count: 'exact' })
      .eq('is_public', true);

    const publicCharacterCount = publicCharacterCountQuery.count || 0;

    let allPublicCharacters: Char[] = [];
    const BATCH_SIZE = 500;
    for (let i = 0; i < publicCharacterCount; i += BATCH_SIZE) {
      const publicCharactersQuery = await this.supaClient
        .getClient()
        .from('characters')
        .select('id, name, creator_id')
        .eq('is_public', true)
        .range(i, i + BATCH_SIZE - 1)
        .returns<Char[]>();

      allPublicCharacters = allPublicCharacters.concat(
        publicCharactersQuery.data ?? [],
      );
    }

    return allPublicCharacters;
  }

  public async getProfileWithBots(publicBots: Char[]) {
    const creatorIdSet = new Set(publicBots.map((bot) => bot.creator_id));
    const creatorIDs = [...creatorIdSet];

    let creators: Creator[] = [];
    const BATCH_SIZE = 50;

    for (let i = 0; i < creatorIDs.length; i += BATCH_SIZE) {
      const creatorsQuery = await this.supaClient
        .getClient()
        .from('user_profiles')
        .select('id, name, user_name')
        .in('id', creatorIDs.slice(i, i + BATCH_SIZE))
        .returns<Creator[]>();

      creators = creators.concat(creatorsQuery.data ?? []);
    }

    return creators;
  }

  public async getTags() {
    return this.supaClient.tagsMap();
  }
}
