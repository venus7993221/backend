import { Controller, Get, Logger, Res } from '@nestjs/common';
import { Response } from 'express';
import { kebabCase } from 'lodash';
import { SitemapStream, streamToPromise } from 'sitemap';
import { SitemapService } from './sitemap.service';

@Controller('sitemap')
export class SitemapController {
  private readonly logger = new Logger(SitemapController.name);
  private sitemapTimeoutMs = 6 * 3600 * 1000; // 6 hours
  private sitemapXmlCache: Buffer | undefined;

  constructor(private readonly sitemapService: SitemapService) {}

  @Get('/sitemap.xml')
  async getSitemap(@Res() res: Response) {
    res.set('Content-Type', 'text/xml');

    if (this.sitemapXmlCache) {
      this.logger.debug('Read sitemap from cache.');
      res.send(this.sitemapXmlCache);
      return;
    }

    this.logger.debug('Start to build sitemap!');
    const now = new Date();
    const smStream = new SitemapStream({
      hostname: 'https://venusai.chat',
    });

    smStream.write({
      url: '/',
      lastmod: now,
      changefreq: 'always',
    });
    smStream.write({
      url: 'search',
      lastmod: now,
      changefreq: 'always',
    });

    const publicCharacters = await this.sitemapService.getAllPublicBots();
    publicCharacters.forEach((character) => {
      smStream.write({
        url: `characters/${character.id}_character-${kebabCase(
          character.name,
        )}`,
        lastmod: now,
        changefreq: 'daily',
      });
    });
    this.logger.debug(
      `Sitemap with ${publicCharacters.length} public characters!`,
    );

    const profiles = await this.sitemapService.getProfileWithBots(
      publicCharacters,
    );
    profiles.forEach((profile) => {
      smStream.write({
        url: `profiles/${profile.id}_profile-of-${kebabCase(
          profile.user_name || profile.name,
        )}`,
        lastmod: now,
        changefreq: 'daily',
      });
    });
    this.logger.debug(`Sitemap with ${profiles.length} creator!`);

    const tags = await this.sitemapService.getTags();
    Object.values(tags).forEach((tag) => {
      smStream.write({
        url: `tags/${tag.id}_characters-with-tag-${tag.slug}`,
        lastmod: now,
        changefreq: 'daily',
      });
    });

    const staticPages = ['login', 'register', 'policy', 'term', 'faq'];
    staticPages.forEach((page) => {
      smStream.write({
        url: page,
        changefreq: 'monthly',
      });
    });

    smStream.end();

    const xml = await streamToPromise(smStream);
    this.sitemapXmlCache = xml;
    setTimeout(() => {
      this.logger.debug('Sitemap cache cleared!');
      this.sitemapXmlCache = undefined;
    }, this.sitemapTimeoutMs);

    res.send(xml);
  }
}
