import {
  Body,
  CacheKey,
  CacheTTL,
  Controller,
  Delete,
  Get,
  HttpException,
  Logger,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  SupabaseJWTGuard,
  SupabaseJWTGuardOptional,
} from 'src/auth/supabase.guard';
import { GetUserId } from 'src/auth/user.decorator';
import { CharacterService } from 'src/character/character.service';
import {
  CharacterDto,
  CharacterEntity,
  CharacterLite,
  CharacterView,
  CheckCharacterParams,
  FullCharacterView,
  SearchCharactersParams,
} from 'src/character/character.dto';
import { Paginated } from 'src/shared/paginated.dto';
import { HttpStatusCode } from 'axios';
import { RealIP } from 'nestjs-real-ip';
import { HttpCacheInterceptor } from 'src/shared/httpCache.interceptor';
import { CacheManagerService } from 'src/shared/cache-manager.service';
import { CacheInterceptor } from '@nestjs/cache-manager';
import { TEN_MINUTE_TTL } from 'src/shared/const';

const SEARCH_CACHE_KEY = 'character_search';

@Controller('characters')
@ApiTags('characters')
export class CharacterController {
  private readonly logger = new Logger(CharacterController.name);

  constructor(
    private characterService: CharacterService,
    private cacheManagerService: CacheManagerService,
  ) {}

  @Get('')
  @UseInterceptors(HttpCacheInterceptor)
  @CacheKey(SEARCH_CACHE_KEY)
  @CacheTTL(TEN_MINUTE_TTL) // 10 minutes
  @ApiResponse({ type: Paginated<CharacterView> })
  async searchCharacters(@Query() params: SearchCharactersParams) {
    return this.characterService.searchPublicCharacters(params);
  }

  @Post()
  @ApiBearerAuth()
  @ApiResponse({ type: CharacterEntity })
  @UseGuards(SupabaseJWTGuard)
  async createCharacter(
    @RealIP() ip: string,
    @GetUserId() userId: string,
    @Body() character: CharacterDto,
  ) {
    // Enable this in case you want to log ip for preventing spam
    // this.logger.log('Bot created', { ip, ...character });

    // throw new HttpException(
    //   'Server error, please try again',
    //   HttpStatusCode.ServiceUnavailable,
    // );

    const createdCharacter = await this.characterService.createCharacter(
      userId,
      character,
    );

    if (createdCharacter?.is_public) {
      this.cacheManagerService.clearCachePrefix(SEARCH_CACHE_KEY + '-page=1'); // Clear page 1 cache only lol
    }

    return createdCharacter;
  }

  @Patch('/:id')
  @ApiBearerAuth()
  @ApiResponse({ type: CharacterEntity })
  @UseGuards(SupabaseJWTGuard)
  async updateCharacter(
    @GetUserId() userId: string,
    @Param('id') id: string,
    @Body() character: CharacterDto,
  ) {
    const updatedCharacter = await this.characterService.updateCharacter(
      id,
      userId,
      character,
    );
    this.cacheManagerService.clearCharacterCache(id);
    return updatedCharacter;
  }

  @Get('/:id')
  @ApiResponse({ type: FullCharacterView })
  @UseGuards(SupabaseJWTGuardOptional)
  @ApiBearerAuth()
  @UseInterceptors(CacheInterceptor)
  @CacheTTL(TEN_MINUTE_TTL) // 10 minutes
  getCharacter(@Param('id') characterId: string, @GetUserId() userId?: string) {
    return this.characterService.getCharacter(characterId, userId);
  }

  @Delete(':id')
  @ApiResponse({ type: FullCharacterView, isArray: true })
  @UseGuards(SupabaseJWTGuard)
  async deleteCharacter(@Param('id') id: string, @GetUserId() userId: string) {
    await this.characterService.deleteCharacter(id, userId);

    this.cacheManagerService.clearCharacterCache(id);
    return { success: true };
  }

  // Make this a post because body can be longgg
  @Post('/check')
  @ApiResponse({ type: CharacterLite, isArray: true })
  async checkCharacter(@Body() params: CheckCharacterParams) {
    return this.characterService.checkCharacter(params);
  }
}
