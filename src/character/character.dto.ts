import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDateString,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { TagEntity } from 'src/tag/tag.dto';

export class CharacterDto {
  @IsNotEmpty()
  @IsString()
  avatar: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  personality: string;

  @IsString()
  scenario: string;

  @IsString()
  description: string;

  @IsString()
  example_dialogs: string;

  @IsNotEmpty()
  @IsString()
  first_message: string;

  @IsNotEmpty()
  @IsBoolean()
  is_nsfw: boolean;

  @IsNotEmpty()
  @IsBoolean()
  is_public: boolean;

  @IsNumber({}, { each: true })
  tag_ids: number[];
}

export class CharacterStats {
  chat: number;
  message: number;
  // Review etc later lol
}

export class CharacterView {
  id: string;
  name: string;
  avatar: string;
  description: string;
  @IsDateString()
  created_at: string;
  is_public: boolean;
  is_nsfw: boolean;
  creator_id: string;
  creator_name: string;
  creator_verified: boolean;
  tags?: TagEntity[];

  stats?: CharacterStats; // Populated later
}

export class FullCharacterView extends CharacterView {
  example_dialogs: string;
  first_message: string;
  personality: string;
  scenario: string;
}

export class CharacterEntity {
  id: string;
  avatar: string;
  created_at: string;
  creator_id: string;
  description: string;
  example_dialogs: string;
  first_message: string;
  is_nsfw: boolean;
  is_public: boolean;
  name: string;
  personality: string;
  scenario: string;
  updated_at: string;
}

export class SearchCharactersParams {
  @IsString()
  @IsOptional()
  user_id?: string;

  @IsNumber()
  @Type(() => Number)
  @IsOptional()
  tag_id?: number;

  @IsNumber()
  @Type(() => Number)
  @IsNotEmpty()
  page: number;

  @IsString()
  @IsIn(['sfw', 'all', 'nsfw'])
  @IsOptional()
  mode?: 'sfw' | 'all' | 'nsfw';

  @IsString()
  @IsOptional()
  @IsIn(['latest', 'popular'])
  sort?: 'latest' | 'popular';

  @IsString()
  @IsIn(['trending', 'newcomer'])
  @IsOptional()
  special_mode?: 'trending' | 'newcomer'; // Some special mode for front-page, custom filter & sort

  @IsString()
  @IsOptional()
  search?: string;
}

export class CheckCharacterParams {
  @IsString()
  name: string;

  @IsString()
  personality: string;
}

// Can not use type because type/interface not available at runtime lol
export class CharacterLite {
  id: string;
  name: string;
  avatar: string;
  description: string;
  personality: string;
  creator_id: string;
  creator_name: string;
  creator_verified: boolean;
  is_nsfw: boolean;
}
