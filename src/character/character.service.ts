import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  OnApplicationBootstrap,
} from '@nestjs/common';
import Fuse from 'fuse.js';
import moment from 'moment';

import {
  CharacterDto,
  CharacterLite,
  CharacterView,
  CheckCharacterParams,
  SearchCharactersParams,
} from 'src/character/character.dto';
import { SupaClient } from 'src/shared/supabaseClient';
import {
  DEFAULT_PAGE_SIZE,
  getPagination,
  Paginated,
} from 'src/shared/paginated.dto';

import { ImageService } from 'src/shared/image.service';
import { Cron, CronExpression } from '@nestjs/schedule';

const TRASH_CREATOR_ID = 'fc672ef1-a22c-4178-8933-088a29228416'; // Prod

@Injectable()
export class CharacterService implements OnApplicationBootstrap {
  private readonly logger = new Logger(CharacterService.name);
  private fuseInstance: Fuse<CharacterLite> | undefined = undefined;

  constructor(
    private supaClient: SupaClient,
    private imageService: ImageService,
  ) {}

  async createCharacter(creatorId: string, payload: CharacterDto) {
    const character = await this.supaClient
      .getClient()
      .from('characters')
      .insert({
        avatar: payload.avatar,
        name: payload.name,
        personality: payload.personality,
        description: payload.description,
        scenario: payload.scenario,
        first_message: payload.first_message,
        example_dialogs: payload.example_dialogs,
        creator_id: creatorId,
        is_nsfw: payload.is_nsfw,
        is_public: payload.is_public,
      })
      .select()
      .single();

    if (character.data && payload.tag_ids.length > 0) {
      // Only get first 5
      const tag_ids = payload.tag_ids.slice(0, 5);

      await this.supaClient.getCharacterTags().insert(
        tag_ids.map((tag_id) => ({
          character_id: character.data.id,
          tag_id,
        })),
      );
    }

    // Just run asynchonus for faster lol
    this.imageService.syncImageToLocal('bot-avatars', payload.avatar).then();

    return character.data;
  }

  async updateCharacter(id: string, creatorId: string, payload: CharacterDto) {
    const result = await this.supaClient
      .getClient()
      .from('characters')
      .update({
        avatar: payload.avatar,
        name: payload.name,
        personality: payload.personality,
        description: payload.description,
        scenario: payload.scenario,
        first_message: payload.first_message,
        example_dialogs: payload.example_dialogs,
        creator_id: creatorId,
        is_nsfw: payload.is_nsfw,
        is_public: payload.is_public,
        updated_at: new Date().toISOString(),
      })
      .match({ id, creator_id: creatorId })
      .select()
      .single();

    // Just delete and re-create lol
    await this.supaClient.getCharacterTags().delete().eq('character_id', id);
    // Only get first 5
    const tag_ids = payload.tag_ids.slice(0, 5);
    await this.supaClient.getCharacterTags().insert(
      tag_ids.map((tag_id) => ({
        character_id: id,
        tag_id,
      })),
    );

    // Just run asynchonus for faster lol
    this.imageService.syncImageToLocal('bot-avatars', payload.avatar).then();

    return result.data;
  }

  async searchPublicCharacters(
    searchParams: SearchCharactersParams,
  ): Promise<Paginated<CharacterView>> {
    const { user_id, mode, tag_id, page, sort, search, special_mode } =
      searchParams;
    const { from, to } = getPagination(page);

    let baseQuery = this.supaClient
      .getClient()
      .from('character_search')
      .select(
        'id, name, avatar, created_at, updated_at, creator_id, creator_name, creator_verified, description, is_nsfw, is_public, tag_ids, total_chat, total_message',
        { count: 'exact' },
      )
      .eq('is_public', true)
      .range(from, to - 1);

    if (user_id) {
      baseQuery = baseQuery.eq('creator_id', user_id);
    }
    if (mode) {
      if (mode === 'nsfw') {
        baseQuery = baseQuery.eq('is_nsfw', true);
      } else if (mode === 'sfw') {
        baseQuery = baseQuery.eq('is_nsfw', false);
      }
    }

    // Special mode will override sort some filter, and search
    if (special_mode) {
      if (special_mode === 'trending') {
        const fiveDayAgo = moment().add(-5, 'day');

        baseQuery = baseQuery
          .order('total_message', { ascending: false })
          .gte('created_at', fiveDayAgo.toISOString());
      }
      if (special_mode === 'newcomer') {
        const twentyFourHourAgo = moment().add(-24, 'hour');
        baseQuery = baseQuery
          .order('total_message', { ascending: false })
          .gte('created_at', twentyFourHourAgo.toISOString());
      }
    } else {
      // support more sort later
      if (sort === 'popular') {
        baseQuery = baseQuery.order('total_message', { ascending: false });
      } else {
        baseQuery = baseQuery.order('created_at', { ascending: false });
      }
    }

    if (tag_id) {
      baseQuery = baseQuery.contains('tag_ids', [tag_id as any]);
    }

    if (search) {
      // Shit, this is ugly lol
      const orFilter = `name.ilike.*${search}*,description.ilike.*${search}*,personality.ilike.*${search}*,scenario.ilike.*${search}*,creator_name.ilike.*${search}*`;

      baseQuery = baseQuery.or(orFilter);
    }

    const { data: characterIDsData, count } = await baseQuery;

    if (!count) {
      return { data: [], total: count ?? 0, size: DEFAULT_PAGE_SIZE, page };
    }

    const tagMap = await this.supaClient.tagsMap();

    const charactersWithTagsAndStats = characterIDsData.map((character) => {
      const char = {
        ...character,
        tags: (character.tag_ids || []).map((tag_id) => tagMap[tag_id]),
        stats: {
          chat: character.total_chat ?? 0,
          message: character.total_message ?? 0,
        },
      };
      return char;
    });

    return {
      data: charactersWithTagsAndStats as CharacterView[],
      total: count ?? 0,
      size: DEFAULT_PAGE_SIZE,
      page,
    };
  }

  async getCharacter(characterId: string, userId?: string) {
    let baseQuery = this.supaClient
      .getClient()
      .from('character_search')
      .select(
        'id, name, avatar, created_at, creator_id, creator_name, creator_verified, description, personality, scenario, example_dialogs, first_message, is_nsfw, is_public, tag_ids, total_chat, total_message, updated_at',
      )
      .eq('id', characterId);
    if (userId) {
      // // SQL injection lol
      baseQuery = baseQuery.or('is_public.eq.true,creator_id.eq.' + userId);
    } else {
      baseQuery = baseQuery.eq('is_public', true);
    }

    const characterData = await baseQuery.limit(1).single();
    if (characterData.error) {
      this.logger.error('Can not find character', characterData.error);
    }

    if (!characterData.data) {
      throw new HttpException('Can not find character', HttpStatus.NOT_FOUND);
    }

    const tagMap = await this.supaClient.tagsMap();
    const character = characterData.data;
    // Refactor this later
    const characterWithTagAndStats = {
      ...character,
      tags: (character.tag_ids || []).map((tag_id) => tagMap[tag_id]),
      stats: {
        chat: character.total_chat ?? 0,
        message: character.total_message ?? 0,
      },
    };

    return characterWithTagAndStats as CharacterView;
  }

  async deleteCharacter(id: string, creatorId: string) {
    const client = this.supaClient.getClient();

    const query = await client
      .from('characters')
      .select('id')
      .match({ id, creator_id: creatorId })
      .select()
      .single();

    const character = query.data;

    if (character === null) {
      throw new HttpException('Can not find character', HttpStatus.NOT_FOUND);
    }

    // Delete all creators chat
    await client
      .from('chats')
      .delete()
      .match({ character_id: id, user_id: creatorId });

    const chatQuery = await client
      .from('chats')
      .select('id', { count: 'exact', head: true }) // head: true mean data not returned anyway
      .eq('character_id', id);

    // If there is any public chat, move character to trash creator
    // Otherwise just remove
    if (chatQuery.count !== null && chatQuery.count > 0) {
      await client
        .from('characters')
        .update({
          is_public: false,
          is_force_remove: true,
          creator_id: TRASH_CREATOR_ID,
        })
        .match({ id, creator_id: creatorId })
        .select()
        .single();
    } else {
      await client.from('characters').delete().match({ id });
    }
  }

  async checkCharacter(params: CheckCharacterParams) {
    if (!this.fuseInstance) {
      this.logger.warn('Fuse instance not initialized');
      return [];
    }

    const result = this.fuseInstance.search({
      $and: [{ name: params.name }, { personality: params.personality }],
    });

    // Get top 2 only
    return result.map((r) => r.item).slice(0, 3);
  }

  @Cron(CronExpression.EVERY_6_HOURS)
  async loadFuseInstance() {
    const publicCharacterCountQuery = await this.supaClient
      .getClient()
      .from('character_search')
      .select('id', { head: true, count: 'exact' })
      .eq('is_public', true);

    const publicCharacterCount = publicCharacterCountQuery.count || 0;
    this.logger.debug(
      `Start to load Fuse instance: Total ${publicCharacterCount} public characters.`,
    );

    let allPublicCharacters: CharacterLite[] = [];
    const BATCH_SIZE = 300;
    for (let i = 0; i < publicCharacterCount; i += BATCH_SIZE) {
      const publicCharactersQuery = await this.supaClient
        .getClient()
        .from('character_search')
        .select(
          'id, name, avatar, description, personality, is_nsfw, creator_id, creator_name, creator_verified',
        )
        .eq('is_public', true)
        .range(i, i + BATCH_SIZE - 1)
        .returns<CharacterLite[]>();

      allPublicCharacters = allPublicCharacters.concat(
        publicCharactersQuery.data ?? [],
      );
    }

    this.fuseInstance = new Fuse(allPublicCharacters, {
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: [
        { name: 'name', weight: 2 },
        { name: 'personality', weight: 1 },
      ],
    });
    this.logger.debug(
      `Finish loading Fuse instance with ${allPublicCharacters.length} public characters.`,
    );
  }

  async onApplicationBootstrap() {
    if (__dirname.includes('Desktop')) {
      this.logger.debug('Local, do not run fuse instance');
      return;
    }

    this.loadFuseInstance();
  }
}
