import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { CacheModule } from '@nestjs/cache-manager';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';

import { AuthModule } from 'src/auth/auth.module';
import { SharedModule } from 'src/shared/shared.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CharacterModule } from './character/character.module';
import { ProfileModule } from './profile/profile.module';
import { ChatModule } from './chat/chat.module';
import { APP_FILTER } from '@nestjs/core';
import { GlobalExceptionFilter } from 'src/filters/GlobalExceptionFilter';
import { TagModule } from './tag/tag.module';
import { TunnelModule } from './tunnel/tunnel.module';
import { SitemapModule } from './sitemap/sitemap.module';
import { ReviewModule } from './review/review.module';

@Global()
@Module({
  imports: [
    HttpModule.register({
      timeout: 60000,
      maxRedirects: 5,
    }),
    SitemapModule,
    ReviewModule,
  ],
  exports: [HttpModule],
})
export class GlobalHttpModule {}

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: process.env.NODE_ENV
        ? `.env.${process.env.NODE_ENV}`
        : '.env',
    }),
    CacheModule.register({
      isGlobal: true,
    }),
    ScheduleModule.forRoot(),
    GlobalHttpModule,
    AuthModule,
    SharedModule,
    CharacterModule,
    ProfileModule,
    ChatModule,
    TagModule,
    TunnelModule,
    HttpModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: GlobalExceptionFilter,
    },
  ],
})
export class AppModule {}
