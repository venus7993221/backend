import { Global, Module } from '@nestjs/common';
import { CacheManagerService } from 'src/shared/cache-manager.service';
import { HttpCacheInterceptor } from 'src/shared/httpCache.interceptor';
import { ImageService } from 'src/shared/image.service';
import { SupaClient } from 'src/shared/supabaseClient';
import { TasksService } from 'src/shared/task.service';

@Global()
@Module({
  imports: [],
  providers: [
    SupaClient,
    TasksService,
    HttpCacheInterceptor,
    CacheManagerService,
    ImageService,
  ],
  exports: [
    SupaClient,
    HttpCacheInterceptor,
    CacheManagerService,
    ImageService,
  ],
})
export class SharedModule {}
