export const DEFAULT_CACHE_TTL = 3600 * 1000; // 1 hour

export const TEN_MINUTE_TTL = 10 * 60 * 1000;
