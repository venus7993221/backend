import * as path from 'path';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import * as download from 'image-downloader';

@Injectable()
export class ImageService {
  private readonly logger = new Logger(ImageService.name);

  constructor(private readonly config: ConfigService) {}

  async syncImageToLocal(folderName: string, url: string) {
    try {
      const imageRootFolder =
        (this.config.getOrThrow('IMG_FOLDER') as string) ||
        path.join(__dirname, 'temp');
      const result = await download.image({
        url: `${this.config.getOrThrow(
          'SUPABASE_URL',
        )}/storage/v1/object/public/${folderName}/${url}`,
        dest: path.join(imageRootFolder, folderName),
      });

      this.logger.debug('Sync image success', result);
    } catch (err) {
      this.logger.error('Failed to sync image', err);
    }
  }
}
