import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, CronExpression } from '@nestjs/schedule';
import { lastValueFrom } from 'rxjs';
import { SupaClient } from 'src/shared/supabaseClient';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    private client: SupaClient,
    private readonly httpService: HttpService,
    private config: ConfigService,
  ) {}

  // @Cron(CronExpression.EVERY_30_SECONDS)
  @Cron(CronExpression.EVERY_10_MINUTES)
  async refreshViewCron() {
    if (
      process.env.NODE_ENV !== 'production' ||
      __dirname.includes('Desktop')
    ) {
      this.logger.log('Cron running locally, doing nothing!');
      return;
    }

    let retryCount = 0;
    while (retryCount <= 3) {
      try {
        console.time('REFRESH character_stats view');
        const result = await this.client
          .getClient()
          .rpc('refresh_materialized_view', { view_name: 'character_stats' });
        console.timeEnd('REFRESH character_stats view');

        if (!result.error) {
          this.logger.log(
            `Refresh materialized view success after ${
              retryCount + 1
            } attempt.`,
          );
          return;
        } else {
          this.logger.error('Refresh materialized view error', result.error);

          retryCount++;
          if (retryCount <= 3) {
            this.logger.log(
              `Retrying the operation. Attempt ${retryCount} of 3`,
            );
            continue;
          } else {
            this.logger.error('Maximum attempts reached. Giving up.');
          }
        }
      } catch (err) {
        this.logger.error('Refresh materialized view error', err);
        retryCount++;
        if (retryCount <= 3) {
          this.logger.log(`Retrying the operation. Attempt ${retryCount} of 3`);
          continue;
        } else {
          this.logger.error('Maximum attempts reached. Giving up.');
        }
      }
    }
  }

  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async buildSitemapCron() {
    try {
      const response = this.httpService.post(
        this.config.getOrThrow('BUILD_HOOK_URL'),
      );

      await lastValueFrom(response);
      this.logger.log('Trigger sitemap build success!');
    } catch (err) {
      this.logger.error('Trigger sitemap build error', err);
    }
  }
}
