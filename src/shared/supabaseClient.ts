import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createClient, SupabaseClient } from '@supabase/supabase-js';
import { Database } from 'src/types/supabase';
import * as _ from 'lodash';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { TagEntity } from 'src/tag/tag.dto';
import { DEFAULT_CACHE_TTL } from 'src/shared/const';

@Injectable()
export class SupaClient {
  #supabaseClient: SupabaseClient<Database>;

  constructor(
    config: ConfigService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {
    this.#supabaseClient = createClient<Database>(
      config.get('SUPABASE_URL') || '',
      config.get('SUPABASE_KEY') || '',
    );
  }

  getClient() {
    return this.#supabaseClient;
  }

  getCharacterTags() {
    return this.#supabaseClient.from('character_tags');
  }

  // Cache for 1-2 hours
  async tagsMap() {
    const key = 'tagsMap';
    let tagsMap = await this.cacheManager.get<_.Dictionary<TagEntity>>(key);

    if (!tagsMap) {
      const result = await this.#supabaseClient.from('tags').select();
      const tags = result.data;

      // Turn data into a map by id and return
      tagsMap = _.keyBy(tags, 'id');

      await this.cacheManager.set(key, tagsMap, DEFAULT_CACHE_TTL);
    }

    return tagsMap;
  }
}
