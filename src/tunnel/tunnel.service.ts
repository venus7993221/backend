import { HttpService } from '@nestjs/axios';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class TunnelService {
  constructor(private readonly httpService: HttpService) {}

  private normalizeUrl(url: string) {
    let trimmedUrl = url.trim();
    if (!trimmedUrl.endsWith('/api')) {
      trimmedUrl += trimmedUrl.endsWith('/') ? 'api' : '/api';
    }
    return trimmedUrl;
  }

  public async checkKoboldUrl(apiUrl: string) {
    try {
      const normalizedUrl = this.normalizeUrl(apiUrl);

      const response = this.httpService.get<{ result: string }>(
        `${normalizedUrl}/v1/model`,
      );

      const result = await lastValueFrom(response);
      return result.data;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }
}
