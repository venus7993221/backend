import { Controller, Get, Query } from '@nestjs/common';
import { TunnelService } from './tunnel.service';

@Controller('tunnel')
export class TunnelController {
  constructor(private readonly tunnelService: TunnelService) {}

  @Get('/kobold/check')
  public checkKoboldUrl(@Query('apiUrl') apiUrl: string) {
    return this.tunnelService.checkKoboldUrl(apiUrl);
  }
}
