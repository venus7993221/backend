import { Database, Json } from './supabase';

export type NonNullableFields<T> = {
  [P in keyof T]: NonNullable<T[P]>;
};

export type SupaChat = Database['public']['Tables']['chats']['Row'];
export type SupaChatMessage =
  Database['public']['Tables']['chat_messages']['Row'];
export type SupaJson = Json;
export type SupaUserProfile =
  Database['public']['Tables']['user_profiles']['Row'];
export type SupaReview = Database['public']['Tables']['reviews']['Row'];

export type SupaStats = Database['public']['Views']['character_stats']['Row'];
