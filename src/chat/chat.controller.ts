import {
  Controller,
  Post,
  Body,
  Patch,
  Param,
  UseGuards,
  Get,
  Delete,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  SupabaseJWTGuard,
  SupabaseJWTGuardOptional,
} from 'src/auth/supabase.guard';
import { GetUserId } from 'src/auth/user.decorator';
import {
  ChatEntity,
  ChatMessageEntity,
  ChatResponse,
  CreateChatDto,
  CreateChatMessageDto,
  DeleteChatMessageDto,
  UpdateChatDto,
  UpdateChatMessageDto,
} from 'src/chat/chat.dto';
import { ChatService } from './chat.service';

@Controller('chats')
@ApiBearerAuth()
@ApiTags('chats')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @Post()
  @ApiResponse({ type: ChatEntity })
  @UseGuards(SupabaseJWTGuard)
  create(@GetUserId() userId: string, @Body() createChatDto: CreateChatDto) {
    return this.chatService.create(userId, createChatDto);
  }

  @Patch(':id')
  @ApiResponse({ type: ChatEntity })
  @UseGuards(SupabaseJWTGuard)
  update(
    @Param('id') chatId: string,
    @GetUserId() userId: string,
    @Body() updateChatDto: UpdateChatDto,
  ) {
    return this.chatService.update(Number(chatId), userId, updateChatDto);
  }

  @Get(':id')
  @ApiResponse({ type: ChatResponse })
  @UseGuards(SupabaseJWTGuardOptional)
  get(@Param('id') chatId: string, @GetUserId() userId?: string) {
    return this.chatService.getChat(Number(chatId), userId);
  }

  // For delete chat, just use the Supabase provided

  @Post(':id/messages')
  @ApiResponse({ type: ChatMessageEntity })
  @UseGuards(SupabaseJWTGuard)
  createMessage(
    @Param('id') chatId: string,
    @GetUserId() userId: string,
    @Body() createChatMessageDto: CreateChatMessageDto,
  ) {
    return this.chatService.createMessage(
      Number(chatId),
      userId,
      createChatMessageDto,
    );
  }

  @Patch(':id/messages/:messageId')
  @UseGuards(SupabaseJWTGuard)
  async updateMessage(
    @Param('id') chatId: string,
    @Param('messageId') messageId: string,
    @GetUserId() userId: string,
    @Body() createChatMessageDto: UpdateChatMessageDto,
  ) {
    await this.chatService.updateMessage(
      Number(chatId),
      Number(messageId),
      userId,
      createChatMessageDto,
    );

    return { success: true };
  }

  @Delete(':id/messages')
  @UseGuards(SupabaseJWTGuard)
  async deleteMessages(
    @Param('id') chatId: string,
    @GetUserId() userId: string,
    @Body() deleteChatMessageDto: DeleteChatMessageDto,
  ) {
    await this.chatService.deleteMessages(
      Number(chatId),
      userId,
      deleteChatMessageDto,
    );

    return { success: true };
  }
}
