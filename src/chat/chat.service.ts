import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import {
  CreateChatDto,
  CreateChatMessageDto,
  DeleteChatMessageDto,
  UpdateChatDto,
  UpdateChatMessageDto,
} from 'src/chat/chat.dto';
import { SupaClient } from 'src/shared/supabaseClient';
import { SupaChat, SupaChatMessage } from 'src/types/type-alias';
import { DEFAULT_CACHE_TTL } from 'src/shared/const';

@Injectable()
export class ChatService {
  private readonly logger = new Logger(ChatService.name);

  constructor(
    private supaClient: SupaClient,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  async create(userId: string, payload: CreateChatDto) {
    const firstMessageResponse = await this.supaClient
      .getClient()
      .from('characters')
      .select('id, first_message')
      .eq('id', payload.character_id)
      .limit(1)
      .single();
    if (!firstMessageResponse.data) {
      throw new HttpException(
        'Can not create chat. Failed to get character',
        HttpStatus.BAD_REQUEST,
      );
    }

    const result = await this.supaClient
      .getClient()
      .from('chats')
      .insert({
        character_id: payload.character_id,
        user_id: userId,
        is_public: false,
      })
      .select()
      .limit(1)
      .single();

    const chat = result.data;

    if (result.error) {
      this.logger.error('Can not create chat', result.error);
    }
    if (!result.data) {
      throw new HttpException('Can not create chat', HttpStatus.BAD_REQUEST);
    }

    // Create first chat massage
    const messageResult = await this.supaClient
      .getClient()
      .from('chat_messages')
      .insert({
        chat_id: result.data?.id,
        is_bot: true,
        is_main: true,
        message: firstMessageResponse.data?.first_message || '',
      })
      .select()
      .single();

    if (messageResult.error) {
      this.logger.error('Can not create chat massage', result.error);
      throw new HttpException('Can not create chat', HttpStatus.BAD_REQUEST);
    }

    return chat;
  }

  async update(chatId: number, userId: string, payload: UpdateChatDto) {
    const result = await this.supaClient
      .getClient()
      .from('chats')
      .update({
        is_public: payload.is_public,
        summary: payload.summary,
        summary_chat_id: payload.summary_chat_id,
        updated_at: new Date().toISOString(),
      })
      .match({ id: chatId, user_id: userId })
      .select()
      .single();

    if (result.error) {
      this.logger.error('can not update chat', result.error);
      throw new HttpException('Can not update chat', HttpStatus.BAD_REQUEST);
    }

    return result.data;
  }

  async getChat(chatId: number, userId?: string) {
    const baseQuery = this.supaClient
      .getClient()
      .from('chats')
      .select(
        '*, characters(id, name, description, avatar, example_dialogs, first_message, personality, scenario)',
      )
      .eq('id', chatId);

    const query = userId
      ? baseQuery.or('is_public.eq.true,user_id.eq.' + userId)
      : baseQuery.eq('is_public', true);

    const chatData = await query.maybeSingle();
    const chat = chatData.data;
    if (!chat) {
      throw new HttpException('Chat not found', HttpStatus.NOT_FOUND);
    }

    const chatMessagesData = await this.supaClient
      .getClient()
      .from('chat_messages')
      .select('*')
      .eq('chat_id', chatId)
      .order('id', { ascending: false })
      .limit(1000);

    const chatMessages = chatMessagesData.data || [];

    return { chat, chatMessages };
  }

  async createMessage(
    chatId: number,
    userId: string,
    payload: CreateChatMessageDto,
  ): Promise<SupaChatMessage | null> {
    const chat = await this._getChatById(chatId);

    if (!chat || chat.user_id !== userId) {
      throw new HttpException('Chat not found', HttpStatus.NOT_FOUND);
    }

    const message = await this.supaClient
      .getClient()
      .from('chat_messages')
      .insert({
        chat_id: chatId,
        is_bot: payload.is_bot,
        is_main: payload.is_main,
        message: payload.message,
      })
      .select()
      .single();

    // Just chat last updated time if user enter a message, do not await, ignore if error lol
    if (!payload.is_bot) {
      this.supaClient
        .getClient()
        .rpc('update_chat', { chat_id: chatId })
        .then((result) => {
          if (result.error) {
            this.logger.error(`Update chat ${chatId} failed`, result.error);
          }
        });

      this.supaClient
        .getClient()
        .from('chats')
        .update({
          updated_at: new Date().toISOString(),
        })
        .match({ id: chatId, user_id: userId })
        .then(() => {
          // console.log(result);
        });
    }

    return message.data;
  }

  // Just return a boolean to save bandwitch lol
  async updateMessage(
    chatId: number,
    messageId: number,
    userId: string,
    payload: UpdateChatMessageDto,
  ): Promise<boolean> {
    const chat = await this._getChatById(chatId);

    if (!chat || chat.user_id !== userId) {
      throw new HttpException('Chat not found', HttpStatus.NOT_FOUND);
    }

    const message = await this.supaClient
      .getClient()
      .from('chat_messages')
      .update({ message: payload.message, is_main: payload.is_main })
      .match({ chat_id: chatId, id: messageId });

    if (message.error) {
      this.logger.error('Can not update massage', message.error);
      throw new HttpException('Can not update message', HttpStatus.BAD_REQUEST);
    }

    return true;
  }

  async deleteMessages(
    chatId: number,
    userId: string,
    payload: DeleteChatMessageDto,
  ): Promise<boolean> {
    const chat = await this._getChatById(chatId);

    if (!chat || chat.user_id !== userId) {
      throw new HttpException('Chat not found', HttpStatus.NOT_FOUND);
    }

    const message = await this.supaClient
      .getClient()
      .from('chat_messages')
      .delete()
      .eq('chat_id', chatId)
      .in('id', payload.message_ids);

    // Ignore if can not delete message, throw nothing lol
    if (message.error) {
      this.logger.error('Can not delete massage', message.error);
    }

    return true;
  }

  private async _getChatById(chatId: number): Promise<SupaChat | undefined> {
    const key = 'CHAT_' + chatId;

    let result = await this.cacheManager.get<SupaChat>(key);
    if (!result) {
      const response = await this.supaClient
        .getClient()
        .from('chats')
        .select('*')
        .eq('id', chatId)
        .single();

      if (response.data) {
        result = response.data;
        this.cacheManager.set(key, response.data, DEFAULT_CACHE_TTL);
      }
    }

    return result;
  }
}
