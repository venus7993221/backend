import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export class CreateChatDto {
  @IsString()
  @IsNotEmpty()
  @IsUUID()
  character_id: string;
}

export class UpdateChatDto {
  @IsBoolean()
  @IsOptional()
  is_public?: boolean;

  @IsString()
  @IsOptional()
  summary?: string;

  @IsNumber()
  @IsOptional()
  summary_chat_id?: number;
}

export class ChatEntity {
  id: number;
  character_id: string;
  user_id: string;
  summary: string;
  created_at: string;
  updated_at: string;
  is_public: boolean;
  summary_chat_id: number | null;
  chat_count: number;
}

export class ChatEntityWithCharacter extends ChatEntity {
  characters: {
    id: string;
    name: string;
    description: string;
    avatar: string;
    example_dialogs: string;
    first_message: string;
    personality: string;
    scenario: string;
  };
}

export class ChatMessageEntity {
  id: number;
  chat_id: number;
  created_at: string;
  is_bot: boolean;
  is_main: boolean;
  message: string;
}

export class ChatResponse {
  chat: ChatEntityWithCharacter;
  chatMessages: ChatMessageEntity[];
}

export class CreateChatMessageDto {
  @IsString()
  @IsNotEmpty()
  message: string;

  @IsBoolean()
  is_bot: boolean;

  @IsBoolean()
  is_main: boolean;
}

export class UpdateChatMessageDto {
  @IsNotEmpty()
  @IsOptional()
  message?: string;

  @IsBoolean()
  @IsOptional()
  is_main?: boolean;
}

export class DeleteChatMessageDto {
  @IsNumber({}, { each: true })
  message_ids: number[];
}
