import { Injectable } from '@nestjs/common';
import { SupaClient } from 'src/shared/supabaseClient';

@Injectable()
export class TagService {
  constructor(private supaClient: SupaClient) {}

  async findAll() {
    const result = await this.supaClient
      .getClient()
      .from('tags')
      .select()
      .order('id', { ascending: true });

    const tags = result.data;

    return tags;
  }
}
