import { CacheInterceptor, CacheKey, CacheTTL } from '@nestjs/cache-manager';
import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { TagEntity } from 'src/tag/tag.dto';
import { TagService } from './tag.service';

@Controller('tags')
@ApiTags('tags')
export class TagController {
  constructor(private readonly tagService: TagService) {}

  @Get()
  @UseInterceptors(CacheInterceptor)
  @CacheKey('tags')
  @CacheTTL(3600 * 1000)
  @ApiResponse({ type: TagEntity, isArray: true })
  findAll() {
    return this.tagService.findAll();
  }
}
