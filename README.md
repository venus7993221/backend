## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

Use `yarn dlx @yarnpkg/sdks vscode` to use TS in VS Code.

## Installation

```bash
yarn install
```

## Running the app

1. Go to `backend-supabase` and run `supabase start`
2. Copy the `.env.example` into `.env`, then replace `SUPABASE_URL`, `SUPABASE_KEY`, `SUPABASE_JWT_SECRET`, `POSTGRES_CONNECTION` value from supabase local
3. Remember to use `service_role key` and not `anon key`
4. On production, create a `.env.production` and fill in the correct data

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
